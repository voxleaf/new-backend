require('./api/v1/config/dbConnection');
const express = require('express'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    expressValidator = require('express-validator'),
    compression = require('compression'),
    helmet = require('helmet'),
    logger = require('morgan'),
    routes = require('./api/v1/routes'),
    config = require('./api/v1/config/config'),
    fs = require('fs'),
    app = express();

app.set('secret', config.secret);

app.use(logger(app.get('env') === 'production' ? 'combined' : 'dev'));


// Added by YY
app.use((req,res,next) =>{
    var now = new Date().toString();
    var log = `${now}: ${req.method} ${req.url}` ;

    fs.appendFile('backend.log' , log + '\n', (err) => {
        if(err){
            console.log('Unable to append file backend.log');
        }
    });

    console.log(log);
    next();
});

//End Here

app.use(cors({
    origin: true,
    credentials: true,
    methods: ['GET', 'POST', 'PATCH', 'DELETE']
}));
app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
// Express Validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        const namespace = param.split('.'),
            root = namespace.shift();
        let formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

app.use('/api/v1', routes);

//any request reaches this point means it failed to match any of the above routes
app.use(function (req, res) {
    res.status(404).json({
        err: null,
        msg: 'The requested route is not defined on the server.',
        data: null
    });
});

module.exports = app;
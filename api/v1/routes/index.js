const express = require('express'),
    router = express.Router(),
    jwt = require('jsonwebtoken'),
    passport = require('../config/passport'),
    authCtrl = require('../controllers/AuthController'),
    resOwnerCtrl = require('../controllers/RestaurantOwnerController'),
    userCtrl = require('../controllers/UserController'),
    smsUserCtrl= require('../controllers/SmsController'),
    adminCtrl = require('../controllers/AdminController'),
    superAdminCtrl = require('../controllers/SuperAdminController');


const isAuthenticated = function (req, res, next) {
    const token = req.headers['authorization'];
    if (token) {
        jwt.verify(token, req.app.get('secret'), function (err, decodedToken) {
            if (err) {
                return res.status(401).json({
                    error: err,
                    msg: 'Login timed out, please login again.',
                    data: null
                });
            }
            req.decodedToken = decodedToken;
            next();
        });
    } else {
        res.status(401).json({
            error: null,
            msg: 'You are not logged in.',
            data: null
        });
    }
};

const isAdmin = function (req, res, next) {
    if (req.decodedToken.admin || req.decodedToken.superAdmin) {
        next();
    } else {
        res.status(403).json({
            error: null,
            msg: 'You are not authorized to do this action.',
            data: null
        });
    }
};

const isSuperAdmin = function (req, res, next) {
    if (req.decodedToken.superAdmin) {
        next();
    } else {
        res.status(403).json({
            error: null,
            msg: 'You are not authorized to do this action.',
            data: null
        });
    }
};

const checkAdminUsersPermissions = function (req, res, next) {
    let allowed;
    switch (req.method) {
        case 'GET':
            allowed = req.decodedToken.user.permissions.users.read;
            break;
        case 'POST':
            allowed = req.decodedToken.user.permissions.users.create;
            break;
        case 'PATCH':
            allowed = req.decodedToken.user.permissions.users.edit;
            break;
        case 'DELETE':
            allowed = req.decodedToken.user.permissions.users.delete;
            break;
    }
    if (allowed) {
        next();
    } else {
        res.status(403).json({
            error: null,
            msg: 'You are not authorized to do this action.',
            data: null
        });
    }
};

const checkAdminOwnersPermissions = function (req, res, next) {
    let allowed;
    switch (req.method) {
        case 'GET':
            allowed = req.decodedToken.user.permissions.owners.read;
            break;
        case 'POST':
            allowed = req.decodedToken.user.permissions.owners.create;
            break;
        case 'PATCH':
            allowed = req.decodedToken.user.permissions.owners.edit;
            break;
        case 'DELETE':
            allowed = req.decodedToken.user.permissions.owners.delete;
            break;
    }
    if (allowed) {
        next();
    } else {
        res.status(403).json({
            error: null,
            msg: 'You are not authorized to do this action.',
            data: null
        });
    }
};

const isNotAuthenticated = function (req, res, next) {
    const token = req.headers['authorization'];
    if (!token) {
        next();
    } else {
        res.status(403).json({
            error: null,
            msg: 'You are already logged in.',
            data: null
        });
    }
};

const isFacebookAuthGranted = function (req, res) {
    if (!req.user) {
        return res.send(401, 'Unauthorized');
    }
    const token = jwt.sign({
        user: req.user
    }, req.app.get('secret'), {
        expiresIn: 7200
    });
    res.status(200).json({
        error: null,
        msg: 'Welcome, ' + req.user.firstname + '.',
        data: {
            token: token,
            user: req.user
        }
    });
};

//<------------------------------------------FB Auth------------------------------------------------->
router.route('/auth/facebookLogin').post(isNotAuthenticated, passport.authenticate('facebook-token', {
    session: false
}), isFacebookAuthGranted);

//<------------------------------------------SMS Auth---------------------------------------------------->
router.route('/auth/smsUserSignup').post(isNotAuthenticated, authCtrl.smsUserSignup);
router.route('/auth/smsUserLogin').post(isNotAuthenticated, authCtrl.smsUserLogin);
router.route('/auth/smsResendUserPassword').post(isNotAuthenticated, authCtrl.smsResendUserPassword);
//<------------------------------------------Auth---------------------------------------------------->
router.route('/auth/userSignup').post(isNotAuthenticated, authCtrl.userSignup);
router.route('/auth/ownerSignup').post(isNotAuthenticated, authCtrl.ownerSignup);
router.route('/auth/userLogin').post(isNotAuthenticated, authCtrl.userLogin);
router.route('/auth/ownerLogin').post(isNotAuthenticated, authCtrl.ownerLogin);
router.route('/auth/adminLogin').post(isNotAuthenticated, authCtrl.adminLogin);
router.route('/auth/resendVerificationEmail').post(authCtrl.resendVerification);
router.route('/auth/checkVerificationToken/:verificationToken').get(authCtrl.checkVerificationToken);
router.route('/auth/verifyAccount/:userId').post(authCtrl.confirmVerification);
router.route('/auth/forgotPassword').patch(isNotAuthenticated, authCtrl.forgotPassword);
router.route('/auth/checkResetPasswordToken/:accountType/:resetToken').get(isNotAuthenticated, authCtrl.checkResetPasswordToken);
router.route('/auth/resetPassword/:accountType/:userId').patch(isNotAuthenticated, authCtrl.resetPassword);
router.route('/auth/changePassword/:accountType').patch(isAuthenticated, authCtrl.changePassword);


//<------------------------------------------Admin--------------------------------------------------->
router.route('/admin/getSmsCurrentUsers').get(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.getSmsCurrentUsers);
router.route('/admin/getSmsVerifiedUsers').get(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.getSmsVerifiedUsers);
//<------------------------------------------By SMS Users--------------------------------------------------->

router.route('/admin/createUser').post(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.createUser);
router.route('/admin/suspendUser/:userId').delete(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.suspendUser);
router.route('/admin/getUnverifiedUsers').get(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.getUnverifiedUsers);
router.route('/admin/getCurrentUsers').get(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.getCurrentUsers);
router.route('/admin/getUserInfo/:userId').get(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.getUserInfo);
router.route('/admin/editUserInfo/:userId').patch(isAuthenticated, isAdmin, checkAdminUsersPermissions, adminCtrl.editUserInfo);
router.route('/admin/createOwner').post(isAuthenticated, isAdmin, checkAdminOwnersPermissions, adminCtrl.createOwner);
router.route('/admin/suspendOwner/:ownerId').delete(isAuthenticated, isAdmin, checkAdminOwnersPermissions, adminCtrl.suspendOwner);
router.route('/admin/getUnverifiedOwners').get(isAuthenticated, isAdmin, checkAdminOwnersPermissions, adminCtrl.getUnverifiedOwners);
router.route('/admin/getCurrentOwners').get(isAuthenticated, isAdmin, checkAdminOwnersPermissions, adminCtrl.getCurrentOwners);
router.route('/admin/getOwnerInfo/:ownerId').get(isAuthenticated, isAdmin, checkAdminOwnersPermissions, adminCtrl.getOwnerInfo);
router.route('/admin/editOwnerInfo/:ownerId').patch(isAuthenticated, isAdmin, checkAdminOwnersPermissions, adminCtrl.editOwnerInfo);

//<------------------------------------------Super Admin--------------------------------------------->
router.route('/superAdmin/createAdmin').post(isAuthenticated, isSuperAdmin, superAdminCtrl.createAdmin);
router.route('/superAdmin/suspendAdmin/:adminId').delete(isAuthenticated, isSuperAdmin, superAdminCtrl.suspendAdmin);
router.route('/superAdmin/getCurrentAdmins').get(isAuthenticated, isSuperAdmin, superAdminCtrl.getCurrentAdmins);
router.route('/superAdmin/getAdminInfo/:adminId').get(isAuthenticated, isSuperAdmin, superAdminCtrl.getAdminInfo);
router.route('/superAdmin/editAdminInfo/:adminId').patch(isAuthenticated, isSuperAdmin, superAdminCtrl.editAdminInfo);

//<------------------------------------------SMS User---------------------------------------------------->
router.route('/user/getDetail').get(isAuthenticated, smsUserCtrl.getInfo);
router.route('/user/editDetail').patch(isAuthenticated, smsUserCtrl.editInfo);

//<------------------------------------------User---------------------------------------------------->
router.route('/user/getInfo').get(isAuthenticated, userCtrl.getInfo);
router.route('/user/editInfo').patch(isAuthenticated, userCtrl.editInfo);

//<------------------------------------------Restaurant Owner---------------------------------------->
router.route('/restaurantOwner/getInfo').get(isAuthenticated, resOwnerCtrl.getInfo);
router.route('/restaurantOwner/editInfo').patch(isAuthenticated, resOwnerCtrl.editInfo);


module.exports = router;
const jwt = require('jsonwebtoken'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    Admin = mongoose.model('Admin'),
    User = mongoose.model('User'),
    SmsUser = mongoose.model('SmsUser'),
    RestaurantOwner = mongoose.model('RestaurantOwner'),
    mailer = require('../config/nodemailer'),
    randtoken = require('rand-token'),
    SMS = require('../config/sms'),
    // Create a token generator with the default settings:


    frontendDomain = require('../config/config').frontendDomain;


const checkSignup = function (req, owner, callback) {

    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'email format is not valid.').isEmail();
    req.checkBody('password', 'password is required.').notEmpty();
    req.checkBody('password', 'password must be at least 8 characters.').len(8);
    req.checkBody('confirmPassword', 'Passwords do not match.').equals(req.body.password);

    if (owner) {
        req.checkBody('firstname', 'firstname is required.').notEmpty();
        req.checkBody('lastname', 'lastname is required.').notEmpty();
        req.checkBody('dob', 'dob is required.').notEmpty();
        req.checkBody('username', 'username is required.').notEmpty();
        req.checkBody('city', 'city is required.').notEmpty();
        req.checkBody('postalCode', 'postalCode is required.').notEmpty();
        req.checkBody('mobileNumber', 'mobileNumber is required.').notEmpty();
        req.checkBody('delivery', 'delivery is required.').notEmpty();
        req.checkBody('cuisine', 'cuisine is required.').notEmpty();
        req.checkBody('role', 'role is required.').notEmpty();
        req.checkBody('restaurantName', 'restaurantName is required.').notEmpty();
    }

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            callback(errors.array(), null);
        } else {
            req.body.email = (req.body.email + '').toLowerCase().trim();
            if (req.body.mobileNumber) {
                req.body.mobileNumber = (req.body.mobileNumber + '').trim();
                const mobRegex = new RegExp('^07[0-9]{9}$');
                if (!mobRegex.test(req.body.mobileNumber))
                    return callback(null, '11 digit mobile number must start with 07 ( 0712123456744 or 0731234567823 )');
            }
            if (req.body.username) {
                req.body.username = req.body.username.toLowerCase().trim();
            }
            const passwordRegex = new RegExp('(?=^.{8,}$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[a-z]).*$');
            if (!passwordRegex.test(req.body.password))
                return callback(null, 'Password must contain letters, numbers and special characters.');

            let orArray = [{
                email: req.body.email
            }];
            if (req.body.username) {
                orArray.push({
                    username: req.body.username
                });
            }
            Admin.findOne({
                $or: orArray
            }, function (err, admin) {
                if (err)
                    return callback(err, null);
                if (admin) {
                    let msg = 'Email already exists, Email: ' + admin.email + '. Please enter another email.';
                    if (admin.username == req.body.username)
                        msg = 'Username already exists, Username: ' + admin.username + '. Please choose another username.';
                    callback(null, msg);
                } else {
                    User.findOne({
                        email: req.body.email
                    }, function (err, user) {
                        if (err)
                            return callback(err, null);
                        if (user) {
                            let msg = 'Email already exists, Email: ' + user.email + '. Please enter another email.';
                            //check on mobileNumber
                            if(req.body.mobileNumber)
                                if (user.mobileNumber === req.body.mobileNumber) {
                                    msg = 'Mobile Number already exists, Mobile Number: ' + user.mobileNumber + '. Please enter another mobile number.';
                                }
                            callback(null, msg);
                        } else {
                            orArray = [{
                                mobileNumber: req.body.mobileNumber
                            }, {
                                email: req.body.email
                            }];
                            if (owner) {
                                orArray.push({
                                    username: req.body.username
                                });
                            }
                            RestaurantOwner.findOne({
                                $or: orArray
                            }, function (err, resOwner) {
                                if (err)
                                    return callback(err, null);
                                if (resOwner) {
                                    let msg = 'Email already exists, Email: ' + resOwner.email + '. Please enter another email.';
                                    //check on mobileNumber
                                    if (resOwner.mobileNumber === req.body.mobileNumber)
                                        msg = 'Mobile Number already exists, Mobile Number: ' + resOwner.mobileNumber + '. Please enter another Mobile Number.';
                                    else if (resOwner.username == req.body.username)
                                        msg = 'Username already exists, Username: ' + resOwner.username + '. Please choose another username.';
                                    callback(null, msg);
                                } else {
                                    //security measure to protect against form injection of critical fields
                                    delete req.body.verified;
                                    delete req.body.createdAt;
                                    delete req.body.updatedAt;
                                    delete req.body.profilePicture;
                                    delete req.body.resetPasswordToken;
                                    delete req.body.resetPasswordTokenExpiry;
                                    callback(null, null);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

const updateUser = (user,req) => {
    return new Promise((resolve, reject) => {

        req.body.verified = true;
        delete req.body.password;
        delete req.body.email;
        delete req.body.resetPasswordToken;
        delete req.body.resetPasswordTokenExpiry;
        delete req.body.createdAt;
        req.body.updatedAt = moment().toDate();

        SmsUser.findByIdAndUpdate(user._id, {
            $set: req.body
        }, {
            new: true
        }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
            .exec(function (err, user) {
                if (err) {
                    reject({
                        statusCode: 500,
                        error: err,
                        msg: null,
                        data: null
                    });
                }else if (!user)
                    reject({
                        statusCode: 404,
                        error: null,
                        msg: 'User was not found.',
                        data: null
                    });
                else {
                    resolve({
                        statusCode : 200,
                        error: null,
                        msg: 'Your information was updated successfully.',
                        data: user
                    });
                }
            });
    });
}


const checkSmsSignup = function (req, owner, callback) {

    req.checkBody('mobileNumber', 'mobile number is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobile number format is not valid.').len(10);

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            callback(errors.array(), null);
        } else {

            req.body.mobileNumber = (req.body.mobileNumber + '').trim();
            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return callback(null, '11 digit mobile number must start with 07 ( 0712123456744 or 0731234567823 )');

            let orArray = [{
                mobileNumber: req.body.mobileNumber
            }];

            Admin.findOne({
                $or: orArray
            }, function (err, admin) {
                if (err)
                    return callback(err, null);
                if (admin) {
                    let msg = 'Mobile Number already exists, Mobile: ' + admin.mobileNumber + '. Please enter another number.';
                    callback(null, msg);
                } else {
                    SmsUser.findOne({
                        mobileNumber: req.body.mobileNumber
                    }, function (err, user) {
                        if (err)
                            return callback(err, null);
                        if (user) {
                            let msg = 'Mobile Number already exists, Mobile:  ' + user.mobileNumber + '. Please enter another Mobile Number.';
                            callback(null, msg);
                        } else {
                            orArray = [{ // At this point leaving now for later.
                                mobileNumber: req.body.mobileNumber
                            }, {
                                email: req.body.email
                            }];
                            console.log('here before callback');
                            //security measure to protect against form injection of critical fields
                            delete req.body.verified;
                            delete req.body.createdAt;
                            delete req.body.updatedAt;
                            delete req.body.profilePicture;
                            delete req.body.resetPasswordToken;
                            delete req.body.resetPasswordTokenExpiry;
                            callback(null, null);
                        }
                    });
                }
            });
        }
    });
};



module.exports.smsUserSignup = function (req, res) {
    checkSmsSignup(req, false, function (err, msg) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (msg)
            return res.status(422).json({
                error: null,
                msg: msg,
                data: null
            });
        const smsuser = new SmsUser(req.body);
        const token = randtoken.generate(48);
        const password = randtoken.generate(5, '0123456789'); // Generating Random Pincode to send SMS for Login.

        SMS.sendSms('447557357157',`Your code is ${password}`).then((ress)=>{

            smsuser.password = password;
            smsuser.verificationToken = token;
            smsuser.verificationTokenExpiry = moment().add(24, 'hours').toDate();

            smsuser.save(function (err) {
                if (err)
                    return res.status(500).json({
                        error: err,
                        msg: null,
                        data: null
                    });
                res.status(201).json({
                    error: null,
                    msg: 'You registered successfully, Pin Code has been sent to your registered mobile number: "' + smsuser.mobileNumber + '".',
                    data: null
                });
            }); //Save user if sms success.

        }, (errorMessage) => { // Error if error on sending sms.
            smsuser.remove(function (err) {
                res.status(500).json({
                    error: err,
                    msg: errorMessage,
                    data: null
                });
            });
        });

    });
};


const checkSmsUserExist = function (req, owner, callback) {

    req.checkBody('mobileNumber', 'mobile number is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobile number format is not valid.').len(10);

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            callback(errors.array(), null);
        } else {

            req.body.mobileNumber = (req.body.mobileNumber + '').trim();
            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return callback(null, '11 digit mobile number must start with 07 ( 0712123456744 or 0731234567823 )');

            let orArray = [{
                mobileNumber: req.body.mobileNumber
            }];

            SmsUser.findOne({
                mobileNumber: req.body.mobileNumber
            }, function (err, user) {
                if (err)
                    return callback(err, null);
                if (user) {
                    callback(null, null);
                } else {
                    let msg = 'User doesn\'t exist with provided mobile number.';
                    callback(null, msg);
                }
            });
        }
    });
};

module.exports.smsResendUserPassword = function (req, res) {
    checkSmsUserExist(req, false, function (err, msg) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (msg)
            return res.status(422).json({
                error: null,
                msg: msg,
                data: null
            });


        const password = randtoken.generate(5, '0123456789'); // Generating Random Pincode to send SMS for Login.

        SmsUser.findOne({ mobileNumber: req.body.mobileNumber }).exec(function(err, user){
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            if (!user)
                return res.status(404).json({
                    error: null,
                    msg: 'User was not found.',
                    data: null
                });

            SMS.sendSms('447557357157',`Your code is ${password}`).then((ress)=>{
                user.password = password;
                user.resetPasswordToken = undefined;
                user.resetPasswordTokenExpiry = undefined;
                user.save(function (err) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'Your new pin code has been sent to your mobile number.',
                        data: user
                    });
                });



            }, (errorMessage) => { // Error if error on sending sms.

                res.status(500).json({
                    error: err,
                    msg: errorMessage,
                    data: null
                });

            });



        });

    });
};


module.exports.userSignup = function (req, res) {
    checkSignup(req, false, function (err, msg) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (msg)
            return res.status(422).json({
                error: null,
                msg: msg,
                data: null
            });
        const user = new User(req.body);
        const token = randtoken.generate(48);
        user.verificationToken = token;
        user.verificationTokenExpiry = moment().add(24, 'hours').toDate();
        user.save(function (err) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            const html = '<p>Hello ' + user.email + ', <br><br>Welcome, Please verify your account by clicking this <a href="' + frontendDomain + '/verify/' + token + '">Link</a>.<br><br>If you are unable to do so, copy and paste the following link into your browser:<br><br>' + frontendDomain + '/verify/' + token + '</p>';
            const subject = 'Account Verification';
            mailer.sendEmail(subject, user.email, '', html, function (err) {
                if (err)
                    user.remove(function (err) {
                        res.status(500).json({
                            error: err,
                            msg: 'Email address is not valid, registration failed.',
                            data: null
                        });
                    });
                else {
                    res.status(201).json({
                        error: null,
                        msg: 'You registered successfully, A verification email has been sent to your registered email: "' + user.email + '".',
                        data: null
                    });
                }
            });
        });
    });
};


module.exports.ownerSignup = function (req, res) {
    checkSignup(req, true, function (err, msg) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (msg)
            return res.status(422).json({
                error: null,
                msg: msg,
                data: null
            });
        const owner = new RestaurantOwner(req.body);
        owner.save(function (err) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            res.status(201).json({
                error: null,
                msg: 'You registered successfully, your account will still be disabled until one of the admins approve your application.',
                data: null
            });
        });
    });
};


module.exports.checkVerificationToken = function (req, res) {
    User.findOne({
        verificationToken: req.params.verificationToken,
        verificationTokenExpiry: {
            $gt: moment().toDate()
        },
        verified: false
    }, function (err, user) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (!user)
            return res.status(403).json({
                error: null,
                msg: 'Your verification token is invalid or has expired.',
                data: null
            });
        res.status(200).json({
            error: null,
            msg: 'Welcome Aboard, You can now verify your account.',
            data: {
                userId: user._id,
                token: req.params.verificationToken
            }
        });
    });
};


module.exports.confirmVerification = function (req, res) {
    User.findByIdAndUpdate(req.params.userId, {
        $set: {
            verified: true,
            verificationToken: undefined,
            verificationTokenExpiry: undefined
        }
    }, {
        new: true
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, user) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not verify your account right now.',
                    data: null
                });
            if (!user)
                return res.status(404).json({
                    error: null,
                    msg: 'Account was not found.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: 'Account was verified successfully.',
                data: user
            });
        });
};


module.exports.resendVerification = function (req, res) {

    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'Enter a valid email.').isEmail();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            const email = (req.body.email + '').toLowerCase().trim();

            User.findOne({
                email: email,
                verified: false
            }, function (err, user) {
                if (err)
                    return res.status(500).json({
                        error: err,
                        msg: null,
                        data: null
                    });
                if (!user)
                    return res.status(422).json({
                        error: null,
                        msg: 'The email you provided was not used for registration to the platform, or the account associated to it, has already been verified.',
                        data: null
                    });
                //generate a token and assign it to the temp user, expires in 24 hrs
                const token = randtoken.generate(48);
                user.verificationToken = token;
                user.verificationTokenExpiry = moment().add(24, 'hours').toDate();
                user.save(function (err) {
                    if (err) {
                        res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    } else {
                        const html = '<p>Hello ' + user.firstname + ', <br><br>Welcome, Please verify your account by clicking this <a href="' + frontendDomain + '/verify/' + token + '">Link</a>.<br><br>If you are unable to do so, copy and paste the following link into your browser:<br><br>' + frontendDomain + '/verify/' + token + '</p>';
                        const subject = 'Account Verification';
                        mailer.sendEmail(subject, user.email, '', html, function (err) {
                            if (err)
                                user.remove(function (err) {
                                    res.status(500).json({
                                        error: err,
                                        msg: 'Email address is not valid, registration failed.',
                                        data: null
                                    });
                                });
                            else {
                                res.status(200).json({
                                    error: null,
                                    msg: 'A verification email has been sent to your registered email: "' + user.email + '"',
                                    data: null
                                });
                            }
                        });
                    }
                });
            });
        }
    });
};


// sms pin code login
module.exports.smsUserLogin = function (req, res) {

    req.checkBody('mobileNumber', 'mobile number is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobile number format is not valid.').len(10);
    req.checkBody('password', 'pin code is required.').notEmpty();
    req.checkBody('password', 'pin code length is 5 digit.').len(5);

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {

            req.body.mobileNumber = (req.body.mobileNumber + '').trim();
            req.body.password = (req.body.password + '').trim();

            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber)) // Checking if it's starting with 07 by Regex help.
                return res.status(422).json({
                    err: null,
                    msg: '11 digit mobile number must start with 07 ( 0712123456723 or 0731234567844 )',
                    data: null
                });

            SmsUser.findOne({
                mobileNumber: req.body.mobileNumber
            })
                .select('-resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, user) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!user)
                        return res.status(404).json({
                            error: null,
                            msg: 'User was not found.',
                            data: null
                        });
                    user.comparePassword(req.body.password, function (err, isMatch) {
                        if (err)
                            return res.status(500).json({
                                error: err,
                                msg: null,
                                data: null
                            });
                        if (!isMatch)
                            return res.status(401).json({
                                error: null,
                                msg: 'Pin Code is incorrect.',
                                data: null
                            });
                        user = user.toObject();
                        delete user.password;
                        const token = jwt.sign({
                            user: user
                        }, req.app.get('secret'), {
                            expiresIn: 7200
                        });

                        // Updating User after successful mobile number and pin code verification.

                        updateUser(user,req).then((response) =>{
                            console.log(response);

                            res.status(200).json({
                                error: null,
                                msg: 'Welcome, ' + (user.firstname? user.firstname : user.mobileNumber) + '.',
                                data: {
                                    token: token,
                                    user: user
                                }
                            });

                        },(errors) =>{
                            return res.status(errors.statusCode).json(errors);
                        });
                        // Updating User after successful mobile number and pin code verification.

                    });
                });
        }
    });
};



module.exports.userLogin = function (req, res) {

    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'Enter a valid email.').isEmail();
    req.checkBody('password', 'password is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            req.body.email = (req.body.email + '').toLowerCase().trim();

            User.findOne({
                email: req.body.email
            })
                .select('-resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, user) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!user)
                        return res.status(404).json({
                            error: null,
                            msg: 'User was not found.',
                            data: null
                        });
                    user.comparePassword(req.body.password, function (err, isMatch) {
                        if (err)
                            return res.status(500).json({
                                error: err,
                                msg: null,
                                data: null
                            });
                        if (!isMatch)
                            return res.status(401).json({
                                error: null,
                                msg: 'Password is incorrect.',
                                data: null
                            });
                        user = user.toObject();
                        delete user.password;
                        const token = jwt.sign({
                            user: user
                        }, req.app.get('secret'), {
                            expiresIn: 7200
                        });
                        res.status(200).json({
                            error: null,
                            msg: 'Welcome, ' + (user.firstname? user.firstname : user.email) + '.',
                            data: {
                                token: token,
                                user: user
                            }
                        });
                    });
                });
        }
    });
};


module.exports.ownerLogin = function (req, res) {

    if (req.body.email) {
        req.checkBody('email', 'Enter a valid email.').isEmail();
    } else if (!req.body.username) {
        req.checkBody('email', 'email is required.').notEmpty();
        req.checkBody('username', 'username is required.').notEmpty();
    }
    req.checkBody('password', 'password is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            const orArray = [];
            if (req.body.email) {
                req.body.email = (req.body.email + '').toLowerCase().trim();
                orArray.push({
                    email: req.body.email
                });
            }
            if (req.body.username) {
                req.body.username = (req.body.username + '').toLowerCase().trim();
                orArray.push({
                    username: req.body.username
                });
            }
            RestaurantOwner.findOne({
                $or: orArray
            })
                .select('-resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, owner) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!owner)
                        return res.status(404).json({
                            error: null,
                            msg: 'Owner was not found.',
                            data: null
                        });
                    owner.comparePassword(req.body.password, function (err, isMatch) {
                        if (err)
                            return res.status(500).json({
                                error: err,
                                msg: null,
                                data: null
                            });
                        if (!isMatch)
                            return res.status(401).json({
                                error: null,
                                msg: 'Password is incorrect.',
                                data: null
                            });
                        owner = owner.toObject();
                        delete owner.password;
                        const token = jwt.sign({
                            user: owner,
                            resOwner: true
                        }, req.app.get('secret'), {
                            expiresIn: 7200
                        });
                        res.status(200).json({
                            error: null,
                            msg: 'Welcome, ' + owner.firstname + '.',
                            data: {
                                token: token,
                                user: owner,
                                resOwner: true
                            }
                        });
                    });
                });
        }
    });
};


module.exports.adminLogin = function (req, res) {
    if (req.body.email) {
        req.checkBody('email', 'Enter a valid email.').isEmail();
    } else if (!req.body.username) {
        req.checkBody('email', 'email is required.').notEmpty();
        req.checkBody('username', 'username is required.').notEmpty();
    }
    req.checkBody('password', 'password is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            const orArray = [];
            if (req.body.email) {
                req.body.email = (req.body.email + '').toLowerCase().trim();
                orArray.push({
                    email: req.body.email
                });
            }
            if (req.body.username) {
                req.body.username = (req.body.username + '').toLowerCase().trim();
                orArray.push({
                    username: req.body.username
                });
            }
            Admin.findOne({
                $or: orArray
            })
                .select('-resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, admin) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!admin) {
                        return res.status(404).json({
                            error: null,
                            msg: 'Admin account was not found.',
                            data: null
                        });
                    }
                    admin.comparePassword(req.body.password, function (err, isMatch) {
                        if (err)
                            return res.status(500).json({
                                error: err,
                                msg: null,
                                data: null
                            });
                        if (!isMatch)
                            return res.status(401).json({
                                error: null,
                                msg: 'Password is incorrect.',
                                data: null
                            });
                        admin = admin.toObject();
                        delete admin.password;
                        const token = jwt.sign({
                            user: admin,
                            admin: true,
                            superAdmin: admin.superAdmin
                        }, req.app.get('secret'), {
                            expiresIn: 7200
                        });
                        res.status(200).json({
                            error: null,
                            msg: 'Welcome, ' + admin.firstname + '.',
                            data: {
                                token: token,
                                user: admin
                            }
                        });
                    });
                });
        }
    });
};


const forgotPasswordHelper = function (Model, username, email) {
    return new Promise(function (resolve, reject) {
        const orArray = [];
        if (email) {
            orArray.push({
                email: email
            });
        }
        if (username) {
            orArray.push({
                username: username
            });
        }
        Model.findOne({
            $or: orArray
        }, function (err, document) {
            if (err)
                reject(err);
            if (!document)
                return resolve(null);
            const token = randtoken.generate(48);
            document.resetPasswordToken = token;
            document.resetPasswordTokenExpiry = moment().add(24, 'hours').toDate();
            document.save(function (err) {
                if (err)
                    return reject(err);
                const subject = 'Password Reset';
                const html = '<p>A reset password request has been made, please reset the password to your account by clicking this <a href="' + frontendDomain + '/auth/resetPassword/' + document.constructor.modelName + '/' + token + '">Link</a>.<br>If you are unable to do so, copy and paste the following link into your browser:<br><br>' + frontendDomain + '/auth/resetPassword/' + document.constructor.modelName + '/' + token + '<br><br>If you did not make the request then ignore this email and your password will remain unchanged.</p>';
                mailer.sendEmail(subject, document.email, '', html, function (err) {
                    if (err)
                        return reject(err);
                    resolve(document.email);
                });
            });
        });
    });
};


module.exports.forgotPassword = function (req, res) {

    if (req.body.email) {
        req.checkBody('email', 'Enter a valid email.').isEmail();
    } else if (!req.body.username) {
        req.checkBody('email', 'email is required.').notEmpty();
        req.checkBody('username', 'username is required.').notEmpty();
    }
    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            let email = null;
            if (req.body.email) {
                email = (req.body.email + '').toLowerCase().trim();
            } else if (req.body.username) {
                req.body.username = (req.body.username + '').toLowerCase().trim();
            }
            forgotPasswordHelper(User, req.body.username, email).then(function (docEmail) {
                if (docEmail) {
                    res.status(200).json({
                        error: null,
                        msg: 'An Email with further instructions on password reset was sent successfully to "' + docEmail + '", check your inbox!',
                        data: null
                    });
                } else {
                    forgotPasswordHelper(RestaurantOwner, req.body.username, email).then(function (docEmail) {
                        if (docEmail) {
                            res.status(200).json({
                                error: null,
                                msg: 'An Email with further instructions on password reset was sent successfully to "' + docEmail + '", check your inbox!',
                                data: null
                            });
                        } else {
                            forgotPasswordHelper(Admin, req.body.username, email).then(function (docEmail) {
                                if (docEmail) {
                                    return res.status(200).json({
                                        error: null,
                                        msg: 'An Email with further instructions on password reset was sent successfully to "' + docEmail + '", check your inbox!',
                                        data: null
                                    });
                                }
                                res.status(404).json({
                                    error: null,
                                    msg: 'Email or Username is not associated with any existing account.',
                                    data: null
                                });
                            }).catch(function (err) {
                                res.status(500).json({
                                    error: err,
                                    msg: 'Could not reset your password, please try again.',
                                    data: null
                                });
                            });
                        }
                    }).catch(function (err) {
                        res.status(500).json({
                            error: err,
                            msg: 'Could not reset your password, please try again.',
                            data: null
                        });
                    });
                }
            }).catch(function (err) {
                res.status(500).json({
                    error: err,
                    msg: 'Could not reset your password, please try again.',
                    data: null
                });
            });
        }
    });
};


module.exports.checkResetPasswordToken = function (req, res) {
    const Model = req.params.accountType === 'Admin' ? Admin : (req.params.accountType === 'RestaurantOwner' ? RestaurantOwner : (req.params.accountType === 'User' ? User : null));
    if (!Model)
        return res.status(422).json({
            error: null,
            msg: 'A valid accountType param is required.',
            data: null
        });
    Model.findOne({
        resetPasswordToken: req.params.resetToken,
        resetPasswordTokenExpiry: {
            $gt: moment().toDate()
        }
    }, function (err, document) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (!document)
            return res.status(422).json({
                error: null,
                msg: 'Your password reset request is invalid or has expired, please make a new request to reset your password.',
                data: null
            });
        res.status(200).json({
            error: null,
            msg: 'Welcome ' + document.firstname + ', You may now change you password.',
            data: {
                userId: document._id,
                token: req.params.resetToken,
            }
        });
    });
};


module.exports.resetPassword = function (req, res) {

    req.checkBody('password', 'password is required.').notEmpty();
    req.checkBody('confirmPassword', 'confirmPassword is required.').notEmpty();
    req.checkBody('password', 'password must be at least 8 characters.').len(8);
    req.checkBody('confirmPassword', 'Passwords do not match.').equals(req.body.password);

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            const Model = req.params.accountType === 'Admin' ? Admin : (req.params.accountType === 'RestaurantOwner' ? RestaurantOwner : (req.params.accountType === 'User' ? User : null));
            if (!Model)
                return res.status(422).json({
                    error: null,
                    msg: 'A valid accountType param is required.',
                    data: null
                });
            const password = req.body.password;
            Model.findById(req.params.userId, function (err, document) {
                if (err)
                    return res.status(500).json({
                        error: err,
                        msg: null,
                        data: null
                    });
                if (!document)
                    return res.status(404).json({
                        error: null,
                        msg: 'Account was not found.',
                        data: null
                    });
                document.password = password;
                document.resetPasswordToken = undefined;
                document.resetPasswordTokenExpiry = undefined;
                document.save(function (err) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'Password was changed successfully.',
                        data: null
                    });
                });
            });
        }
    });
};


module.exports.changePassword = function (req, res) {
    req.checkBody('oldPassword', 'oldPassword is required.').notEmpty();
    req.checkBody('password', 'password is required.').notEmpty();
    req.checkBody('password', 'password must be at least 8 characters.').len(8);
    req.checkBody('confirmPassword', 'Passwords do not match.').equals(req.body.password);

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            const Model = req.params.accountType === 'Admin' ? Admin : (req.params.accountType === 'RestaurantOwner' ? RestaurantOwner : (req.params.accountType === 'User' ? User : null));
            if (!Model)
                return res.status(422).json({
                    error: null,
                    msg: 'A valid accountType param is required.',
                    data: null
                });
            const oldPassword = req.body.oldPassword;
            const password = req.body.password;

            Model.findById(req.decodedToken.user._id, function (err, document) {
                if (err)
                    return res.status(500).json({
                        error: err,
                        msg: null,
                        data: null
                    });
                if (!document)
                    return res.status(404).json({
                        error: null,
                        msg: 'Account was not found.',
                        data: null
                    });
                document.comparePassword(oldPassword, function (err, isMatched) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!isMatched)
                        return res.status(422).json({
                            error: null,
                            msg: 'Your current password is not correct.',
                            data: null
                        });

                    const regex = new RegExp('^' + oldPassword + '$');
                    if (regex.test(password))
                        return res.status(424).json({
                            error: null,
                            msg: 'You can not change your password to the currently existing one.',
                            data: null
                        });

                    document.password = password;
                    document.save(function (err) {
                        if (err)
                            return res.status(500).json({
                                error: err,
                                msg: null,
                                data: null
                            });
                        res.status(200).json({
                            error: null,
                            msg: 'Password was changed successfully.',
                            data: null
                        });
                    });
                });
            });
        }
    });
};
const mongoose = require('mongoose'),
    RestaurantOwner = mongoose.model('RestaurantOwner'),
    moment = require('moment');


module.exports.getInfo = function (req, res) {
    RestaurantOwner.findById(req.decodedToken.user._id)
        .select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, owner) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            if (!owner)
                return res.status(404).json({
                    error: null,
                    msg: 'Owner was not found.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: null,
                data: owner
            });
        });
};


module.exports.editInfo = function (req, res) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('dob', 'dob is required.').notEmpty();
    req.checkBody('restaurantName', 'restaurantName is required.').notEmpty();
    req.checkBody('cuisine', 'cuisine is required.').notEmpty();
    req.checkBody('city', 'city is required.').notEmpty();
    req.checkBody('postalCode', 'postalCode is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobileNumber is required.').notEmpty();
    req.checkBody('role', 'role is required.').notEmpty();
    req.checkBody('delivery', 'delivery is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            req.body.mobileNumber = (req.body.mobileNumber + '').trim();
            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return res.status(422).json({
                    err: null,
                    msg: '11 digit mobile number must start with 07 ( 0712123456723 or 0731234567844 )',
                    data: null
                });
            delete req.body.verified;
            delete req.body.password;
            delete req.body.username;
            delete req.body.email;
            delete req.body.resetPasswordToken;
            delete req.body.resetPasswordTokenExpiry;
            delete req.body.createdAt;
            req.body.updatedAt = moment().toDate();
            RestaurantOwner.findByIdAndUpdate(req.decodedToken.user._id, {
                $set: req.body
            }, {
                new: true
            }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, owner) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!owner)
                        return res.status(404).json({
                            error: null,
                            msg: 'Owner was not found.',
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'Your information was updated successfully.',
                        data: owner
                    });
                });
        }
    });
};
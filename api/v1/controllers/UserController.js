const mongoose = require('mongoose'),
    User = mongoose.model('User'),
    moment = require('moment');


module.exports.getInfo = function (req, res) {
    User.findById(req.decodedToken.user._id)
        .select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, user) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            if (!user)
                return res.status(404).json({
                    error: null,
                    msg: 'User was not found.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: null,
                data: user
            });
        });
};


module.exports.editInfo = function (req, res) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('dob', 'dob is required.').notEmpty();
    req.checkBody('addressLine1', 'addressLine1 is required.').notEmpty();
    req.checkBody('city', 'city is required.').notEmpty();
    req.checkBody('postalCode', 'postalCode is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobileNumber is required.').notEmpty();
    req.checkBody('gender', 'gender is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            req.body.mobileNumber = (req.body.mobileNumber + '').trim();
            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return res.status(422).json({
                    err: null,
                    msg: '11 digit mobile number must start with 07 ( 0712123456723 or 0731234567844 )',
                    data: null
                });
            req.body.verified = true;
            delete req.body.password;
            delete req.body.email;
            delete req.body.resetPasswordToken;
            delete req.body.resetPasswordTokenExpiry;
            delete req.body.createdAt;
            req.body.updatedAt = moment().toDate();
            User.findByIdAndUpdate(req.decodedToken.user._id, {
                $set: req.body
            }, {
                new: true
            }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, user) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!user)
                        return res.status(404).json({
                            error: null,
                            msg: 'User was not found.',
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'Your information was updated successfully.',
                        data: user
                    });
                });
        }
    });
};
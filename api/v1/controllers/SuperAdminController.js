const mongoose = require('mongoose'),
    Admin = mongoose.model('Admin'),
    User = mongoose.model('User'),
    RestaurantOwner = mongoose.model('User'),
    moment = require('moment');


module.exports.getCurrentAdmins = function (req, res) {
    Admin.find({
        _id: {
            $ne: req.decodedToken.user._id
        },
        superAdmin: false
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, admins) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not retrieve current admins right now.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: 'Retrieved current admins successfully.',
                data: admins
            });
        });
};


const checkAdmin = function (req, callback) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('username', 'username is required.').notEmpty();
    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'email format is not valid.').isEmail();
    req.checkBody('password', 'password is required.').notEmpty();
    req.checkBody('password', 'password must be at least 8 characters.').len(8);
    req.checkBody('confirmPassword', 'Passwords do not match.').equals(req.body.password);

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            callback(errors.array(), null);
        } else {
            req.body.email = (req.body.email + '').toLowerCase().trim();
            req.body.username = (req.body.username + '').toLowerCase().trim();
            const passwordRegex = new RegExp('(?=^.{8,}$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[a-z]).*$');
            if (!passwordRegex.test(req.body.password))
                return callback(null, 'Password must contain letters, numbers and special characters.');

            Admin.findOne({
                $or: [{
                    email: req.body.email
                }, {
                    username: req.body.username
                }]
            }, function (err, admin) {
                if (err)
                    return callback(err, null);
                if (admin) {
                    let msg = 'Email already exists, Email: ' + admin.email + '. Please enter another email.';
                    if (admin.username == req.body.username)
                        msg = 'Username already exists, Username: ' + admin.username + '. Please choose another username.';
                    callback(null, msg);
                } else {
                    User.findOne({
                        email: req.body.email
                    }, function (err, user) {
                        if (err)
                            return callback(err, null);
                        if (user) {
                            const msg = 'Email already exists, Email: ' + user.email + '. Please enter another email.';
                            callback(null, msg);
                        } else {
                            RestaurantOwner.findOne({
                                $or: [{
                                    email: req.body.email
                                }, {
                                    username: req.body.username
                                }]
                            }, function (err, resOwner) {
                                if (err)
                                    return callback(err, null);
                                if (resOwner) {
                                    let msg = 'Email already exists, Email: ' + resOwner.email + '. Please enter another email.';
                                    if (resOwner.username == req.body.username)
                                        msg = 'Username already exists, Username: ' + resOwner.username + '. Please choose another username.';
                                    callback(null, msg);
                                } else {
                                    //security measure to protect against form injection of critical fields
                                    delete req.body.createdAt;
                                    delete req.body.updatedAt;
                                    delete req.body.resetPasswordToken;
                                    delete req.body.resetPasswordTokenExpiry;
                                    callback(null, null);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};


module.exports.createAdmin = function (req, res) {
    checkAdmin(req, function (err, msg) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (msg)
            return res.status(422).json({
                error: null,
                msg: msg,
                data: null
            });
        const admin = new Admin(req.body);
        admin.save(function (err) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            res.status(201).json({
                error: null,
                msg: 'Admin was created successfully.',
                data: null
            });
        });
    });
};


module.exports.getAdminInfo = function (req, res) {
    Admin.findById(req.params.adminId)
        .select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, admin) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            if (!admin)
                return res.status(404).json({
                    error: null,
                    msg: 'Admin was not found.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: null,
                data: admin
            });
        });
};


module.exports.editAdminInfo = function (req, res) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('superAdmin', 'superAdmin is required.').notEmpty();
    req.checkBody('permissions', 'permissions is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            delete req.body.resetPasswordToken;
            delete req.body.resetPasswordTokenExpiry;
            delete req.body.createdAt;
            req.body.updatedAt = moment().toDate();
            Admin.findByIdAndUpdate(req.params.adminId, {
                $set: req.body
            }, {
                new: true
            }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, admin) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!admin)
                        return res.status(404).json({
                            error: null,
                            msg: 'Admin was not found.',
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'Admin was updated successfully.',
                        data: admin
                    });
                });
        }
    });
};


module.exports.suspendAdmin = function (req, res) {
    Admin.findByIdAndRemove(req.params.adminId, function (err, admin) {
        if (err)
            return res.status(500).json({
                error: null,
                msg: 'Could not suspend this account right now.',
                data: null
            });
        if (!admin)
            return res.status(404).json({
                error: null,
                msg: 'Account was not found.',
                data: null
            });
        res.status(200).json({
            error: null,
            msg: 'Admin account was suspended successfully.',
            data: null
        });
    });
};
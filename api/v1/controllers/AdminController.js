const mongoose = require('mongoose'),
    User = mongoose.model('User'),
    SmsUser = mongoose.model('SmsUser'),
    Admin = mongoose.model('Admin'),
    RestaurantOwner = mongoose.model('RestaurantOwner'),
    moment = require('moment');

// ------------------------------------------- SMS User Management by Admin ------------------ //

module.exports.getSmsCurrentUsers = function (req, res) {

    SmsUser.find({
        verified: true
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, users) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not retrieve current users right now.',
                    data: null
                });

            res.writeHead(301,
                {Location: 'http://google.com'}
            );
            res.end();

            res.status(200).json({
                error: null,
                msg: 'Retrieved current users successfully.',
                data: users
            });
        });
};

module.exports.getSmsVerifiedUsers = function (req, res) {

    SmsUser.find({
        verified: false
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, users) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not retrieve unverified users right now.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: 'Retrieved unverified users successfully.',
                data: users
            });
        });
};

module.exports.suspendSmsUser = function (req, res) {

    SmsUser.findByIdAndRemove(req.params.userId, function (err, user) {
        if (err)
            return res.status(500).json({
                error: null,
                msg: 'Could not suspend this account right now.',
                data: null
            });
        if (!user)
            return res.status(404).json({
                error: null,
                msg: 'Account was not found.',
                data: null
            });
        res.status(200).json({
            error: null,
            msg: 'Account was suspended successfully.',
            data: null
        });
    });

};

module.exports.getSmsUserInfo = function (req, res) {

    SmsUser.findById(req.params.userId)
        .select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, user) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            if (!user)
                return res.status(404).json({
                    error: null,
                    msg: 'User was not found.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: null,
                data: user
            });
        });
};

module.exports.createSmsUser = function (req, res) {

};


module.exports.editSmsUser = function (req, res) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('dob', 'dob is required.').notEmpty();
    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'email format is not valid.').isEmail();
    req.checkBody('addressLine1', 'addressLine1 is required.').notEmpty();
    req.checkBody('city', 'city is required.').notEmpty();
    req.checkBody('postalCode', 'postalCode is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobileNumber is required.').notEmpty();
    req.checkBody('gender', 'gender is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            req.body.mobileNumber = (req.body.mobileNumber + '').trim();

            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return res.status(422).json({
                    err: null,
                    msg: '11 digit mobile number must start with 07 ( 0712123456722 or 0731234567855 )',
                    data: null
                });
            delete req.body.verified;
            delete req.body.resetPasswordToken;
            delete req.body.resetPasswordTokenExpiry;
            delete req.body.createdAt;
            req.body.updatedAt = moment().toDate();
            SmsUser.findByIdAndUpdate(req.params.userId, {
                $set: req.body
            }, {
                new: true
            }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, user) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!user)
                        return res.status(404).json({
                            error: null,
                            msg: 'User was not found.',
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'User was updated successfully.',
                        data: user
                    });
                });
        }
    });
};


// ------------------------------------------- End SMS User ------------------ //



module.exports.getCurrentUsers = function (req, res) {
    User.find({
        verified: true
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, users) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not retrieve current users right now.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: 'Retrieved current users successfully.',
                data: users
            });
        });
};


module.exports.getUnverifiedUsers = function (req, res) {
    User.find({
        verified: false
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, users) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not retrieve unverified users right now.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: 'Retrieved unverified users successfully.',
                data: users
            });
        });
};


module.exports.suspendUser = function (req, res) {
    User.findByIdAndRemove(req.params.userId, function (err, user) {
        if (err)
            return res.status(500).json({
                error: null,
                msg: 'Could not suspend this account right now.',
                data: null
            });
        if (!user)
            return res.status(404).json({
                error: null,
                msg: 'Account was not found.',
                data: null
            });
        res.status(200).json({
            error: null,
            msg: 'Account was suspended successfully.',
            data: null
        });
    });
};

module.exports.getUserInfo = function (req, res) {
    User.findById(req.params.userId)
        .select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, user) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            if (!user)
                return res.status(404).json({
                    error: null,
                    msg: 'User was not found.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: null,
                data: user
            });
        });
};


const checkAccount = function (req, owner, callback) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('dob', 'dob is required.').notEmpty();
    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'email format is not valid.').isEmail();
    req.checkBody('city', 'city is required.').notEmpty();
    req.checkBody('postalCode', 'postalCode is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobileNumber is required.').notEmpty();
    req.checkBody('password', 'password is required.').notEmpty();
    req.checkBody('password', 'password must be at least 8 characters.').len(8);
    req.checkBody('confirmPassword', 'Passwords do not match.').equals(req.body.password);

    if (!owner) {
        req.checkBody('addressLine1', 'addressLine1 is required.').notEmpty();
        req.checkBody('gender', 'gender is required.').notEmpty();
    } else {
        req.checkBody('username', 'username is required.').notEmpty();
        req.checkBody('delivery', 'delivery is required.').notEmpty();
        req.checkBody('cuisine', 'cuisine is required.').notEmpty();
        req.checkBody('role', 'role is required.').notEmpty();
        req.checkBody('restaurantName', 'restaurantName is required.').notEmpty();
    }

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            callback(errors.array(), null);
        } else {
            req.body.email = (req.body.email + '').toLowerCase().trim();
            req.body.mobileNumber = (req.body.mobileNumber + '').trim();
            if (req.body.username) {
                req.body.username = req.body.username.toLowerCase().trim();
            }
            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return callback(null, '11 digit mobile number must start with 07 ( 0712123456744 or 0731234567823 )');

            const passwordRegex = new RegExp('(?=^.{8,}$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[a-z]).*$');
            if (!passwordRegex.test(req.body.password))
                return callback(null, 'Password must contain letters, numbers and special characters.');

            const orArray = [{
                email: req.body.email
            }];
            if (req.body.username) {
                orArray.push({
                    username: req.body.username
                });
            }
            Admin.findOne({
                $or: orArray
            }, function (err, admin) {
                if (err)
                    return callback(err, null);
                if (admin) {
                    let msg = 'Email already exists, Email: ' + admin.email + '. Please enter another email.';
                    if (admin.username == req.body.username)
                        msg = 'Username already exists, Username: ' + admin.username + '. Please choose another username.';
                    callback(null, msg);
                } else {
                    User.findOne({
                        $or: [{
                            mobileNumber: req.body.mobileNumber
                        }, {
                            email: req.body.email
                        }]
                    }, function (err, user) {
                        if (err)
                            return callback(err, null);
                        if (user) {
                            let msg = 'Email already exists, Email: ' + user.email + '. Please enter another email.';
                            //check on mobileNumber
                            if (user.mobileNumber === req.body.mobileNumber)
                                msg = 'Mobile Number already exists, Mobile Number: ' + user.mobileNumber + '. Please enter another Mobile Number.';
                            callback(null, msg);
                        } else {
                            const orArray = [{
                                mobileNumber: req.body.mobileNumber
                            }, {
                                email: req.body.email
                            }];
                            if (owner) {
                                orArray.push({
                                    username: req.body.username
                                });
                            }
                            RestaurantOwner.findOne({
                                $or: orArray
                            }, function (err, resOwner) {
                                if (err)
                                    return callback(err, null);
                                if (resOwner) {
                                    let msg = 'Email already exists, Email: ' + resOwner.email + '. Please enter another email.';
                                    //check on mobileNumber
                                    if (resOwner.mobileNumber === req.body.mobileNumber)
                                        msg = 'Mobile Number already exists, Mobile Number: ' + resOwner.mobileNumber + '. Please enter another Mobile Number.';
                                    else if (resOwner.username == req.body.username)
                                        msg = 'Username already exists, Username: ' + resOwner.username + '. Please choose another username.';
                                    callback(null, msg);
                                } else {
                                    //security measure to protect against form injection of critical fields
                                    req.body.verified = true;
                                    delete req.body.createdAt;
                                    delete req.body.updatedAt;
                                    delete req.body.profilePicture;
                                    delete req.body.resetPasswordToken;
                                    delete req.body.resetPasswordTokenExpiry;
                                    callback(null, null);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};


module.exports.createUser = function (req, res) {
    checkAccount(req, false, function (err, msg) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (msg)
            return res.status(422).json({
                error: null,
                msg: msg,
                data: null
            });
        const user = new User(req.body);
        user.save(function (err) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            res.status(201).json({
                error: null,
                msg: 'User was created successfully.',
                data: null
            });
        });
    });
};


module.exports.editUserInfo = function (req, res) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('dob', 'dob is required.').notEmpty();
    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'email format is not valid.').isEmail();
    req.checkBody('addressLine1', 'addressLine1 is required.').notEmpty();
    req.checkBody('city', 'city is required.').notEmpty();
    req.checkBody('postalCode', 'postalCode is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobileNumber is required.').notEmpty();
    req.checkBody('gender', 'gender is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            req.body.mobileNumber = (req.body.mobileNumber + '').trim();

            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return res.status(422).json({
                    err: null,
                    msg: '11 digit mobile number must start with 07 ( 0712123456722 or 0731234567855 )',
                    data: null
                });
            delete req.body.verified;
            delete req.body.resetPasswordToken;
            delete req.body.resetPasswordTokenExpiry;
            delete req.body.createdAt;
            req.body.updatedAt = moment().toDate();
            User.findByIdAndUpdate(req.params.userId, {
                $set: req.body
            }, {
                new: true
            }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, user) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!user)
                        return res.status(404).json({
                            error: null,
                            msg: 'User was not found.',
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'User was updated successfully.',
                        data: user
                    });
                });
        }
    });
};


module.exports.getCurrentOwners = function (req, res) {
    RestaurantOwner.find({
        verified: true
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, owners) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not retrieve current owners right now.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: 'Retrieved current owners successfully.',
                data: owners
            });
        });
};


module.exports.getUnverifiedOwners = function (req, res) {
    RestaurantOwner.find({
        verified: false
    }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, owners) {
            if (err)
                return res.status(500).json({
                    error: null,
                    msg: 'Could not retrieve unverified owners right now.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: 'Retrieved unverified owners successfully.',
                data: owners
            });
        });
};


module.exports.createOwner = function (req, res) {
    checkAccount(req, true, function (err, msg) {
        if (err)
            return res.status(500).json({
                error: err,
                msg: null,
                data: null
            });
        if (msg)
            return res.status(422).json({
                error: null,
                msg: msg,
                data: null
            });
        const owner = new RestaurantOwner(req.body);
        owner.save(function (err) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            res.status(201).json({
                error: null,
                msg: 'Owner was created successfully.',
                data: null
            });
        });
    });
};


module.exports.suspendOwner = function (req, res) {
    RestaurantOwner.findByIdAndRemove(req.params.ownerId, function (err, owner) {
        if (err)
            return res.status(500).json({
                error: null,
                msg: 'Could not suspend this account right now.',
                data: null
            });
        if (!owner)
            return res.status(404).json({
                error: null,
                msg: 'Account was not found.',
                data: null
            });
        res.status(200).json({
            error: null,
            msg: 'Account was suspended successfully.',
            data: null
        });
    });
};

module.exports.getOwnerInfo = function (req, res) {
    RestaurantOwner.findById(req.params.ownerId)
        .select('-password -resetPasswordToken -resetPasswordTokenExpiry')
        .exec(function (err, owner) {
            if (err)
                return res.status(500).json({
                    error: err,
                    msg: null,
                    data: null
                });
            if (!owner)
                return res.status(404).json({
                    error: null,
                    msg: 'User was not found.',
                    data: null
                });
            res.status(200).json({
                error: null,
                msg: null,
                data: owner
            });
        });
};

module.exports.editOwnerInfo = function (req, res) {
    req.checkBody('firstname', 'firstname is required.').notEmpty();
    req.checkBody('lastname', 'lastname is required.').notEmpty();
    req.checkBody('dob', 'dob is required.').notEmpty();
    req.checkBody('restaurantName', 'restaurantName is required.').notEmpty();
    req.checkBody('email', 'email is required.').notEmpty();
    req.checkBody('email', 'email format is not valid.').isEmail();
    req.checkBody('cuisine', 'cuisine is required.').notEmpty();
    req.checkBody('city', 'city is required.').notEmpty();
    req.checkBody('postalCode', 'postalCode is required.').notEmpty();
    req.checkBody('mobileNumber', 'mobileNumber is required.').notEmpty();
    req.checkBody('role', 'role is required.').notEmpty();
    req.checkBody('delivery', 'delivery is required.').notEmpty();

    req.getValidationResult().then(function (errors) {

        if (!errors.isEmpty()) {
            res.status(422).json({
                error: errors.array(),
                msg: null,
                data: null
            });
        } else {
            req.body.mobileNumber = (req.body.mobileNumber + '').trim();
            const mobRegex = new RegExp('^07[0-9]{9}$');
            if (!mobRegex.test(req.body.mobileNumber))
                return res.status(422).json({
                    err: null,
                    msg: '11 digit mobile number must start with 07 ( 0712123456723 or 0731234567844 )',
                    data: null
                });
            delete req.body.verified;
            delete req.body.password;
            delete req.body.username;
            delete req.body.resetPasswordToken;
            delete req.body.resetPasswordTokenExpiry;
            delete req.body.createdAt;
            req.body.updatedAt = moment().toDate();
            RestaurantOwner.findByIdAndUpdate(req.params.ownerId, {
                $set: req.body
            }, {
                new: true
            }).select('-password -resetPasswordToken -resetPasswordTokenExpiry')
                .exec(function (err, owner) {
                    if (err)
                        return res.status(500).json({
                            error: err,
                            msg: null,
                            data: null
                        });
                    if (!owner)
                        return res.status(404).json({
                            error: null,
                            msg: 'Owner was not found.',
                            data: null
                        });
                    res.status(200).json({
                        error: null,
                        msg: 'Owner information was updated successfully.',
                        data: owner
                    });
                });
        }
    });
};





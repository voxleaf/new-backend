const mongoose = require('mongoose'),
    bcrypt = require('bcryptjs');

const adminSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        required: true,
        trim: true
    },
    username: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    superAdmin: {
        type: Boolean,
        default: false
    },
    permissions: {
        type: mongoose.SchemaTypes.Mixed,
        default: {
            owners: {
                create: false,
                read: true,
                edit: false,
                delete: false
            },
            users: {
                create: false,
                read: true,
                edit: false,
                delete: false
            }
        }
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
    resetPasswordToken: String,
    resetPasswordTokenExpiry: Date
});


adminSchema.methods.hashPassword = function (password, callback) {
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, function (err, salt) {
        if (err)
            return callback(err, null);
        bcrypt.hash(password, salt, function (err, hash) {
            callback(err, hash);
        });
    });
};


adminSchema.methods.comparePassword = function (candidatePassword, callback) {
    const admin = this;
    bcrypt.compare(candidatePassword, admin.password, function (err, isMatch) {
        callback(err, isMatch);
    });
};


//Middleware to hash password on adding or modification
adminSchema.pre('save', function (next) {
    const admin = this;
    if (!admin.isModified('password'))
        return next();
    admin.hashPassword(admin.password, function (err, hash) {
        if (err)
            return next(err);
        admin.password = hash;
        next();
    });
});


mongoose.model('Admin', adminSchema);
const mongoose = require('mongoose'),
    bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        trim: true
    },
    lastname: {
        type: String,
        trim: true
    },
    dob: Date,
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    addressLine1: {
        type: String,
        trim: true
    },
    addressLine2: {
        type: String,
        trim: true
    },
    city: {
        type: String,
        trim: true
    },
    postalCode: String,
    mobileNumber: {
        type: String,
        trim: true
    },
    password: String,
    gender: {
        type: String,
        trim: true
    },
    facebookId: String,
    verified: {
        type: Boolean,
        default: false
    },
    profilePicture: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: Date,
    verificationToken: String,
    verificationTokenExpiry: Date,
    resetPasswordToken: String,
    resetPasswordTokenExpiry: Date
});


userSchema.methods.hashPassword = function (password, callback) {
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, function (err, salt) {
        if (err)
            return callback(err, null);
        bcrypt.hash(password, salt, function (err, hash) {
            callback(err, hash);
        });
    });
};


userSchema.methods.comparePassword = function (candidatePassword, callback) {
    const user = this;
    bcrypt.compare(candidatePassword, user.password, function (err, isMatch) {
        callback(err, isMatch);
    });
};


//Middleware to hash password on adding or modification
userSchema.pre('save', function (next) {
    const user = this;
    if (!user.isModified('password'))
        return next();
    user.hashPassword(user.password, function (err, hash) {
        if (err)
            return next(err);
        user.password = hash;
        next();
    });
});


mongoose.model('User', userSchema);
const mongoose = require('mongoose'),
    bcrypt = require('bcryptjs');

const smsUserSchema = new mongoose.Schema({
    firstname: {
        type: String,
        trim: true,
        default:null
    },
    lastname: {
        type: String,
        trim: true,
        default:null
    },
    dob: Date,
    email: {
        type: String,
        trim: true,
        lowercase: true,
        default:null
    },
    addressLine1: {
        type: String,
        trim: true,
        default:null
    },
    addressLine2: {
        type: String,
        trim: true,
        default:null
    },
    city: {
        type: String,
        trim: true,
        default:null
    },
    postalCode: {
        type:String,
        default:null
    },
    mobileNumber: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    password: {
        type:String,
        required:true,
    },
    gender: {
        type: String,
        trim: true,
        default:null
    },
    facebookId: String,
    verified: {
        type: Boolean,
        default: false
    },
    profilePicture: {
        type:String,
        default:null
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: Date,
    verificationToken: String,
    verificationTokenExpiry: Date,
    resetPasswordToken: String,
    resetPasswordTokenExpiry: Date
});


smsUserSchema.methods.hashPassword = function (password, callback) {
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, function (err, salt) {
        if (err)
            return callback(err, null);
        bcrypt.hash(password, salt, function (err, hash) {
            callback(err, hash);
        });
    });
};



smsUserSchema.methods.comparePassword = function (candidatePassword, callback) {
    const user = this;
    bcrypt.compare(candidatePassword, user.password, function (err, isMatch) {
        callback(err, isMatch);
    });
};

//Middleware to hash password on adding or modification
smsUserSchema.pre('save', function (next) {
    const user = this;
    if (!user.isModified('password'))
        return next();
    user.hashPassword(user.password, function (err, hash) {
        if (err)
            return next(err);
        user.password = hash;
        next();
    });
});


mongoose.model('SmsUser', smsUserSchema);
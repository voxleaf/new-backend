const mongoose = require('mongoose'),
    bcrypt = require('bcryptjs');
    
const restaurantOwnerSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        required: true,
        trim: true
    },
    username: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    dob: {
        type: Date,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    restaurantName: {
        type: String,
        required: true,
        trim: true
    },
    cuisine: {
        type: String,
        required: true,
        trim: true
    },
    delivery: {
        type: Boolean,
        required: true
    },
    role: {
        type: String,
        required: true,
        trim: true
    },
    city: {
        type: String,
        required: true,
        trim: true
    },
    postalCode: {
        type: String,
        required: true
    },
    mobileNumber: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    verified: {
        type: Boolean,
        default: false
    },
    profilePicture: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: Date,
    resetPasswordToken: String,
    resetPasswordTokenExpiry: Date
});


restaurantOwnerSchema.methods.hashPassword = function (password, callback) {
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, function (err, salt) {
        if(err) 
            return callback(err, null);
        bcrypt.hash(password, salt, function (err, hash) {
            callback(err, hash);
        });
    });
};


restaurantOwnerSchema.methods.comparePassword = function (candidatePassword, callback) {
    const owner = this;
    bcrypt.compare(candidatePassword, owner.password, function (err, isMatch) {
        callback(err, isMatch);
    });
};


//Middleware to hash password on adding or modification
restaurantOwnerSchema.pre('save', function (next) {
    const owner = this;
    if (!owner.isModified('password'))
        return next();
    owner.hashPassword(owner.password, function (err, hash) {
        if (err)
            return next(err);
        owner.password = hash;
        next();
    });
});


mongoose.model('RestaurantOwner', restaurantOwnerSchema);
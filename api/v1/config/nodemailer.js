const nodemailer = require('nodemailer'),
    // mailgun = require('nodemailer-mailgun-transport'),
    mailInfo = require('../config/config').mailer;

// const transporter = nodemailer.createTransport(mailgun(mailInfo.mailgun));

const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: mailInfo.senderEmail,
        pass: mailInfo.password
    }
});

module.exports.sendEmail = function (subject, email, text, html, done) {
    const mailOptions = {
        from: '"' + mailInfo.companyName + '" <' + mailInfo.senderEmail + '>',
        to: email,
        subject: subject,
        text: text,
        html: html
    };

    transporter.sendMail(mailOptions, function (err, info) {
        done(err, info);
    });
};
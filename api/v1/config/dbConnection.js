const mongoose = require('mongoose'),
    config = require('../config/config'),
    dburl = config.MONGO_URI;

mongoose.connect(dburl, {
    useMongoClient: true,
}).then(function () {
    //Do nothing
}).catch(function() {
    console.log('Could not connect to the database');
});

// CAPTURE APP TERMINATION / RESTART EVENTS
// To be called when process is restarted or terminated
function gracefulShutdown(msg, callback) {
    mongoose.connection.close(function () {
        callback();
    });
}


// For nodemon restarts
process.once('SIGUSR2', function () {
    gracefulShutdown('nodemon restart', function () {
        process.kill(process.pid, 'SIGUSR2');
    });
});


// For app termination
process.on('SIGINT', function () {
    gracefulShutdown('App termination (SIGINT)', function () {
        process.exit(0);
    });
});


// For Heroku app termination
process.on('SIGTERM', function () {
    gracefulShutdown('App termination (SIGTERM)', function () {
        process.exit(0);
    });
});

require('../models/Admin');
require('../models/User');
require('../models/RestaurantOwner');
require('../models/smsUser');
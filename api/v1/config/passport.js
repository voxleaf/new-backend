const passport = require('passport'),
    FacebookTokenStrategy = require('passport-facebook-token'),
    User = require('mongoose').model('User'),
    config = require('./config');

passport.use(new FacebookTokenStrategy({
    clientID: config.facebookAppId,
    clientSecret: config.facebookAppSecret,
}, function (accessToken, refreshToken, profile, done) {
    if (!profile.emails[0].value) {
        return done(new Error('email is required.'));
    }
    User.findOne({
        email: profile.emails[0].value
    }, function (err, user) {
        if (err)
            return done(err);
        if (user) {
            if (user.facebookId !== profile.id) {
                return done(new Error('Your Facebook email is already registered in our database.'));
            }
            return done(null, user);
        }
        const newUser = new User({
            facebookId: profile.id,
            firstname: profile.name.givenName,
            lastname: profile.name.familyName,
            email: profile.emails[0].value
        });
        newUser.save(function (err) {
            if (err)
                return done(err);
            return done(null, newUser);
        });
    });
}));

module.exports = passport;
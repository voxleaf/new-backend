const request   = require('request');


var sendSms = (mobile, text) => {
    return new Promise((resolve, reject) => {

        if(mobile.length !=12){
            reject(`Incorrect number supplied ${mobile}`);
        } else if(text.length < 6){
            reject(`Wrong text format provided ${text}`);
        }

        request({
            url:`https://api.txtlocal.com/send/?apikey=lK5/dEwkyOM-27NhDWbOyAB7LyjAIomlNDw75mmlrO&numbers=${mobile}=&sender=kurira&message=${text}`,
            //url : 'https://jsonplaceholder.typicode.com/users',
            json: true,

        },(error, response, body) => {

            //body.status='success';

            if(error) {
                reject('Unable to connect SMS Gateway');
            } else if(body.status=='warnings'){
                reject(body.warnings[0].code + ' ' + body.warnings[0].message) ;
            } else if(body.status==='failure'){
                reject(body.errors[0].code + ' ' + body.errors[0].message) ;
            } else if(body.status==='success') {
                resolve('SMS sent successfully');
            } else {
                reject('try again later.');
            }
        });
    });
};


module.exports.sendSms = sendSms;

module.exports = {
    secret: '!23@23423456jfdbnfsdf#$%^&@*#fsjdbdacv%dvfdshfdbsv@',
    MONGO_URI: process.env.NODE_ENV === 'production' ? '' : 'mongodb://localhost/dialroute',
    mailer: {
        companyName: 'Dialroute',
        senderEmail: 'dialroutetest@gmail.com',//'noreply@dialroute.com',
        password: 'dialroute1234',
        mailgun: {
            auth: {
                api_key: 'key-c9b03067fc86af9994d9d6c362b67971',
                domain: 'https://api.mailgun.net/v3/dialroute.com'
            }
        }
    },
    facebookAppId: '766909860163355',
    facebookAppSecret: 'e8c102ff712a0ecfb7fa5c83f4a06a4f',
    frontendDomain: process.env.NODE_ENV === 'production' ? '' : 'http://localhost:4200'
};